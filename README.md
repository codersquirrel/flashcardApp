This project will is build with Ruby on Rails and Vue.js
It is still in working progress and will be updated with the newest advances soon...

last updated: November 26th, 2019

**Goal**
*  Build a fully functional web application app for vocabulary training, that enables the user to
    * Set up an account, signing in with facebook, google or email
    * Create individual cards, fill in the word, translation and upload a picture from a folder or through a googlesearch
    * Group cards in decks
    * Go through decks and fill out cards, then get the result (right or wrong) and be shown the right answer
    * See their progress through a deck
    * Reset a deck and start again
* Learn to work in a collaboratite and asynchronous fashion in a team of UX Designer and Software Enigneer.


**Approach**

*Set up guidelines for our team*
* How do we want to deal with conflicts and how can we prevent them?
* What communication channels do we want to use?
* What is our goal - for the application and personally?
* How do we keep up to date with our progress?
* What deadlines do we set outselves?
* Formulate questions frequently
* Set up calls regularily

*First steps*
* Set up a Trello Board, a Slack Channel, and Google Docs
* Create a new Project with Ruby on Rails on GitLab
* Draw out a first Roadmap and Schema
* Reach out to colleagues and ask if they are willing to help with design and code review
* What would be the most complex app that we would want to build with all features?
* What is the MVP?


**Responsibilities**
*  Stephanie: UX and UI Design
*  Elisa: Frontend and Backend