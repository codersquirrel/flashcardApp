# frozen_string_literal: true

class CardsController < ApplicationController
  # skip_before_action :authenticate_user!
  def index
    @cards = Card.all
  end

  def new
    @card = Card.new
    # @deck = Deck.find(params[:deck_id])
  end

  def create
    @card = Card.new(card_params)
    # @deck = Deck.find(params[:deck_id])
    # @card.deck = @deck
    if @card.save
      redirect_to cards_path
    else
      render :new
    end
  end

  def show
    @card = Card.find(params[:id])
  end


  def edit
    @card = Card.find(params[:id])
  end

  def update
    @card = Card.find(params[:id])
    if @card.update(card_params)
      redirect_to cards_path
    else
      render :new
    end
  end

  def destroy
    @card = Card.find(params[:id])
    @card.destroy
    redirect_to cards_path
  end


  private

  def card_params
    params.require(:card).permit(:word, :translation, :hint, :deck_id)
    # params.require(:card).permit(:word, :translation, :hint, :photo, :deck_id)
  end
end
