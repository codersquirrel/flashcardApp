class DecksController < ApplicationController
  # skip_before_action :authenticate_user!
  def index
    @decks = Deck.all
  end

  def new
    @deck = Deck.new
  end

  def create
    @deck = Deck.new(deck_params)
    @deck = Deck.find(params[:deck_id])
    if @request.save
      redirect_to requests_path
    else
      render :new
    end
  end

  def show
    @deck = Deck.find(params[:id])
    @card = Card.find(params[:id])
    # @card = @card
  end


  def edit
    @deck = Deck.find(params[:id])
  end

  def update
     @deck = Deck.find(params[:id])
    if @deck.update(card_params)
      redirect_to cards_path
    else
      render :new
    end
  end

  def destroy
    @deck = Deck.find(params[:id])
    @deck.destroy
    redirect_to cards_path
  end

  private

  def deck_params
    params.require(:deck).permit(:name)
  end
end



