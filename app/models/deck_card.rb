class DeckCard < ApplicationRecord
  belongs_to :decks
  belongs_to :cards
  has_many :rounds
end
