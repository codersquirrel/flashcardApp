class Round < ApplicationRecord
  has_many :cards, through: :deck_cards
  has_many :decks, through: :deck_cards
  belongs_to :deck_cards
end
