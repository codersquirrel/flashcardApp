Rails.application.routes.draw do
  get 'rounds/show'
#    Prefix Verb URI Pattern        Controller#Action
#   pages_about GET  /about(.:format)   pages#about
# pages_contact GET  /contact(.:format) pages#contact
#          root GET  /                  pages#home
  devise_for :users, controllers: { registrations: 'users/registrations' }
  root to: 'pages#home'

  resources :decks, only: [:show, :new, :create, :index, :edit, :destroy]

  resources :cards, only: [:show, :new, :create, :index, :edit, :destroy]

  resources :rounds, only: :show

  get 'dashboard', to: 'dashboard#show'

  # cards_show GET    /cards/show(.:format)  cards#show
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
