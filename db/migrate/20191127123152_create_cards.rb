class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.string :word
      t.string :translation
      t.string :picture
      t.text :hint
      t.references :deck, foreign_key: true

      t.timestamps
    end
  end
end
