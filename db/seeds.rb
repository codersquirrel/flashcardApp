# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Card.destroy_all
# Deck.destroy_all

# User.destroy_all

deck1 = Deck.create(
  name: "Fruit"
)

deck2 = Deck.create(
  name: "Vegetables"
)

deck3 = Deck.create(
  name: "Animals"
)

card1 = Card.create(
  word: "Apple",
  translation: "Manzana",
  hint: "Hint",
  picture: "",
  deck_id: deck1.id
)

card2 = Card.create(
  word: "Pear",
  translation: "Birne",
  hint: "Like the light bulb",
  picture: "",
  deck_id: deck1.id
)

card3 = Card.create(
  word: "Grape",
  translation: "Uva",
  hint: "Like the light wave",
  picture: "",
  deck_id: deck1.id
)

card4 = Card.create(
  word: "Banana",
  translation: "Platano",
  hint: "Like a plant",
  picture: "",
  deck_id: deck1.id
)

card5 = Card.create(
  word: "Salad",
  translation: "Lechuga",
  hint: "Starts with an L",
  picture: "",
  deck_id: deck2.id
)

card6 = Card.create(
  word: "Tomato",
  translation: "Jitomate",
  hint: "Almost the same",
  picture: "",
  deck_id: deck2.id
)

card7 = Card.create(
  word: "Carrot",
  translation: "Zanahoria",
  hint: "Starts with a Z",
  picture: "",
  deck_id: deck1.id
)

card8 = Card.create(
  word: "Dog",
  translation: "Perro",
  hint: "Rolling R",
  picture: "",
  deck_id: deck3.id
)

card9 = Card.create(
  word: "Cat",
  translation: "gato",
  hint: "Almost like cat with an o at the end",
  picture: "",
  deck_id: deck3.id
)

